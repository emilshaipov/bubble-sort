package ru.ser.BubbleSort;

import java.util.Arrays;

/**
 *
 * @author Shaipov Emil" 18ИТ18
 *
 */

public class BubbleSort {
    public static void main(String[] args) {
        int[] array = {9, 1, 3, 2, 6, 5, 7, 8, 4};
        bubbleSort(array);
        System.out.println(Arrays.toString(array));
    }

    private static void bubbleSort(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = array.length - 1; j > i; j--) {
                if (array[j - 1] > array[j]) {
                    int tmp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = tmp;
                }
            }
        }
    }
}

